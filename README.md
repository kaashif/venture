Venture ![Build Status](https://api.travis-ci.org/kaashif-hymabaccus/venture.svg?branch=master)
=======

Venture is a game very similar to the "adventure" game included with
many Unix-like operating systems. It works in the same sort of way, too,
but with more advanced line-editing.

Installation
------------
To install, clone this repo and install using cabal:

    $ cabal install

Before long, you'll be able to run it, assuming ~/.cabal/bin is in your
PATH.

    $ venture

Have fun!

Documentation
-------------
To read the documentation, you can just read the source files. If you
want a prettier version, use cabal to build some HTML, using haddock
(which you must have installed).

    $ cabal haddock --executables

After it's done, it will tell you where to find index.html.

Copyright
---------
Copyright (C) 2014 Kaashif Hymabaccus

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this program.  If not, see http://www.gnu.org/licenses/
