{-|
Module      : Player
Copyright   : (c) 2014 Kaashif Hymabaccus
License     : GPL-3
Maintainer  : kaashif@kaashif.co.uk
Stability   : experimental
Portability : POSIX
-}
module Player where

import Vector

-- | Player data structure, storing the current position (and will store inventory, health etc)
data Player = Player { alive :: Bool            -- ^ Whether the player is alive
                     , position :: Vector2D     -- ^ Position on the board (in 2D space)
                     }

-- | The player's condition at the start of the game
initial = Player { alive = True
                 , position = (0,0)
                 }

-- | Moves player from current position to position supplied (validation is done elsewhere)
move :: Player -> Vector2D -> Player
move p (x,y) = p { position = (x,y) }

-- | Moves player 1 \"up\"
north p = move p $ (position p) + (0,1)
-- | Moves player 1 \"right\"
east p = move p $ (position p) + (1,0)
-- | Moves player 1 \"down\"
south p = move p $ (position p) + (0,-1)
-- | Moves player 1 \"left\"
west p = move p $ (position p) + (-1,0)
