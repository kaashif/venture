{-|
Module      : String
Copyright   : (c) 2014 Kaashif Hymabaccus
License     : GPL-3
Maintainer  : kaashif@kaashif.co.uk
Stability   : experimental
Portability : POSIX
-}
module String where

import Data.Char (isSpace)

-- | Trims whitespace from either side of string
trim :: String -> String
trim = trimAndReverse . trimAndReverse
    where trimAndReverse = reverse . dropWhile isSpace

-- | Similar to break, but reverses output
reverseBreak :: (a -> Bool) -> [a] -> ([a], [a])
reverseBreak f xs = (reverse before, reverse after)
    where (after, before) = break f $ reverse xs

-- | Wraps string into a list of lines of <=maxLen length
wrapLine :: Int -> String -> [String]
wrapLine maxLen line 
    | length line <= maxLen  = [line]
    | any isSpace beforeMax  = beforeSpace : (wrapLine maxLen $ afterSpace ++ afterMax)
    | otherwise              = beforeMax : wrapLine maxLen afterMax
        where (beforeMax, afterMax) = splitAt maxLen line
              (beforeSpace, afterSpace) = reverseBreak isSpace beforeMax
