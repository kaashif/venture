{-|
Module      : Vector
Copyright   : (c) 2014 Kaashif Hymabaccus
License     : GPL-3
Maintainer  : kaashif@kaashif.co.uk
Stability   : experimental
Portability : POSIX
-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}

module Vector where

import Data.Monoid

-- | 2D Vector, implementing minimal Num instance (only really useful for + and -)
type Vector2D = (Integer, Integer)

instance Num Vector2D where
    (a,b)+(c,d) = (a+c, b+d)
    a*b = a
    (a,b)-(c,d) = (a-c, b-d)
    negate = id
    signum = id
    abs = id
    fromInteger i = (i,i)
