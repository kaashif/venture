{-|
Module      : Main
Copyright   : (c) 2014 Kaashif Hymabaccus
License     : GPL-3
Maintainer  : kaashif@kaashif.co.uk
Stability   : experimental
Portability : POSIX
-}
module Main where

import System.Directory (removeFile)
import System.IO.Error (isDoesNotExistError)
import System.Console.Haskeline hiding (throwIO, catch)
import Control.Exception (catch, throwIO)
import Control.Monad.Trans.State.Lazy (put, get, modify, StateT, evalStateT)
import Control.Monad.IO.Class (liftIO)
import qualified Player
import qualified Board
import State
import String

-- | The State monad encapsulating the entire game
type Game = StateT State IO

-- | Prints a long help message before starting the game loop
main = putStrLn helpMessage >> initState >>= evalStateT loop

-- | The main game loop, where input and output takes place
loop :: Game ()
loop = do
    saved <- get
    cmd <- liftIO input
    modify (process cmd)
    validateWith saved
    final <- get
    liftIO $ printMessages final
    modify reset
    if (continue final)
        then loop
        else liftIO terminate

-- | Haskeline-powered input, using a file for history storage
input :: IO String
input = runInputT settings inputLoop
    where
        inputLoop :: InputT IO String
        inputLoop = do
            minput <- getInputLine "> "
            case minput of
                Nothing -> return "help"
                Just s -> return s
        settings = defaultSettings { historyFile = Just "/tmp/rogue.tmp" }

-- | Verbose help message, printed on startup
helpMessage = "\
\Welcome to Venture!\n\n\
\  help - display a help message\n\
\  quit - quit the game\n\
\  north,east,south,west - move that way\n\
\  look - look at your surroundings\n"

-- | Cleans up temporary files and exits
terminate :: IO ()
terminate = removeFile "/tmp/rogue.tmp" `catch` handleException
    where handleException e
              | isDoesNotExistError e = return ()
              | otherwise = throwIO e

-- | Replaces state in an invalid Game monad with given (known valid) state
validateWith :: State -> Game ()
validateWith fallback = do
  current <- get
  if (valid current)
     then return ()
     else liftIO (putStrLn "Invalid state!") >> put fallback
